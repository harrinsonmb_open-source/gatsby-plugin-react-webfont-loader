"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _statuses = require("./statuses");

var _statuses2 = _interopRequireDefault(_statuses);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var WebFont = null;

if (typeof window !== 'undefined') {
  WebFont = require('webfontloader');
}

var noop = function noop() {};

var WebfontLoader = function (_React$Component) {
  _inherits(WebfontLoader, _React$Component);

  function WebfontLoader() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, WebfontLoader);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = WebfontLoader.__proto__ || Object.getPrototypeOf(WebfontLoader)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      status: undefined
    }, _this.handleLoading = function () {
      _this.setState({ status: _statuses2.default.loading });
    }, _this.handleActive = function () {
      _this.setState({ status: _statuses2.default.active });
    }, _this.handleInactive = function () {
      _this.setState({ status: _statuses2.default.inactive });
    }, _this.loadFonts = function () {
      if (WebFont !== null) {
        WebFont.load(_extends({}, _this.props.config, {
          loading: _this.handleLoading,
          active: _this.handleActive,
          inactive: _this.handleInactive
        }));
      } else {
        console.info('Webfont is not being loaded, this is fine if you are executing from command line, for example, building a static site generator.');
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(WebfontLoader, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.loadFonts();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      var _props = this.props,
          onStatus = _props.onStatus,
          config = _props.config;


      if (prevState.status !== this.state.status) {
        onStatus(this.state.status);
      }

      if (prevProps.config !== config) {
        this.loadFonts();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var children = this.props.children;

      return children || null;
    }
  }]);

  return WebfontLoader;
}(_react2.default.Component);

WebfontLoader.propTypes = {
  config: _propTypes2.default.object.isRequired,
  children: _propTypes2.default.element,
  onStatus: _propTypes2.default.func.isRequired
};

WebfontLoader.defaultProps = {
  onStatus: noop
};

exports.default = WebfontLoader;